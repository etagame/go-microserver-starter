module go-kit-starter

go 1.12

require (
	github.com/StackExchange/wmi v0.0.0-20190523213609-cbe66965904d // indirect
	github.com/bilibili/kratos v0.3.3
	github.com/gogo/protobuf v1.3.1
	github.com/golang/glog v0.0.0-20170312005925-543a34c32e4d // indirect
	github.com/golang/protobuf v1.3.2
	github.com/grpc-ecosystem/grpc-gateway v1.9.5
	github.com/pkg/errors v0.8.1
	github.com/shirou/w32 v0.0.0-20160930032740-bb4de0191aa4 // indirect
	github.com/urfave/cli v1.22.2 // indirect
	golang.org/x/sys v0.0.0-20191204072324-ce4227a45e2e // indirect
	golang.org/x/time v0.0.0-20190513212739-9d24e82272b4 // indirect
	golang.org/x/xerrors v0.0.0-20190717185122-a985d3407aa7
	google.golang.org/genproto v0.0.0-20191216205247-b31c10ee225f
	google.golang.org/grpc v1.24.0
	gopkg.in/airbrake/gobrake.v2 v2.0.9 // indirect
	gopkg.in/gemnasium/logrus-airbrake-hook.v2 v2.1.2 // indirect
)
