>使用微服务的脚手架,基于bilibili开源的kratos

### 目标

通过此项目，学习微服务的各个模块，并快速搭建部署一个微服务项目

- [x] http基于gin实现,工具名blademaster
- [x] grpc
- [x] proto,并配置自动生成工具
- [x] log 工具
- [ ] Trace 链路追踪
- [x] 服务器发现
- [x] 配置中心config

#### 待定

- [ ] metric 仪表盘
- [ ] ratelimit 节流
- [ ] circuit breaker 熔断器

#### 第三方组件

### 启动

#### 安装依赖 

安装kratos tool

```bash
//go get -u 强制更新到最新
//go get -d 跳过编译和安装
//github 版本升级要符合语义化
go get -u  github.com/bilibili/kratos/tool/kratos

//查看已安装的tool
kratos tool

```

启动

```bash
cd cmd
go run main.go

```


