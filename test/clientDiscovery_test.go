/*
@Desc : 客户单测试服务发现
@Version : 1.0.0
@Time : 2019/7/12 17:40 
@Author : hammercui
@File : clietnDiscovery_test.go
@Company: Sdbean
*/
package test

import (
	"context"
	"fmt"
	"github.com/bilibili/kratos/pkg/naming/discovery"
	"github.com/bilibili/kratos/pkg/net/rpc/warden"
	"github.com/bilibili/kratos/pkg/net/rpc/warden/resolver"
	"go-kit-starter/api"
	"testing"
)

const appId = "starter.service.v1"
// example: discovery://default/account.account.service?cluster=shfy01&cluster=shfy02
const discoveryTarget = "discovery://default/starter.service.v1"
// 客户单服务发现
func TestClientDiscovery(t *testing.T){
	//全局注册
	resolver.Register(discovery.Builder())
	//client配置
	cfg := &warden.ClientConfig{}
	fmt.Println("client cfg:",cfg)
	client  := warden.NewClient(cfg)
	conn, err := client.Dial(context.Background(),discoveryTarget)
	if err!=nil{
		fmt.Println("conn err",err)
		return
	}

	serverClient :=protoModel.NewSearchServiceClient(conn)
	//发送请求
	resp,err := serverClient.Search(context.Background(),&protoModel.SearchReq{Query:"i am client"})
	if err == nil{
		fmt.Println("rpc success,response:",resp)
	}else{
		fmt.Println("rpc fail,err",err)
	}

}