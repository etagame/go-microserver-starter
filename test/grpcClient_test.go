/*
@Desc : 2019/7/5 14:16 
@Version : 1.0.0
@Time : 2019/7/5 14:16 
@Author : hammercui
@File : grpcClient
@Company: Sdbean
*/
package test

import (
	"context"
	"errors"
	"fmt"
	"github.com/bilibili/kratos/pkg/conf/paladin"
	"github.com/bilibili/kratos/pkg/log"
	"github.com/bilibili/kratos/pkg/net/rpc/warden"
	"go-kit-starter/api"
	"golang.org/x/xerrors"
	"os"
	"testing"
)

var target = "0.0.0.0:9000"

func TestSlice(t *testing.T) {
	s := []int{5}

	s = append(s, 7)
	fmt.Println("cap(s) =", cap(s), "ptr(s) =", &s[0])

	s = append(s, 9)
	fmt.Println("cap(s) =", cap(s), "ptr(s) =", &s[0])

	x := append(s, 11)
	fmt.Println("cap(s) =", cap(s), "ptr(s) =", &s[0], "ptr(x) =", &x[0])

	y := append(s, 12)
	fmt.Println("cap(s) =", cap(s), "ptr(s) =", &s[0], "ptr(y) =", &y[0])

	z := append(s, 13, 14)
	fmt.Println("cap(s) =", cap(s), "ptr(s) =", &s[0], "ptr(y) =", &z[0])


}

//测试grpc客户端
func TestGrpcClient(t *testing.T) {
	defaultConfPath := "../configs"
	if client, err := paladin.NewFile(defaultConfPath); err == nil {
		paladin.DefaultClient = client
	} else {
		if err := paladin.Init(); err != nil {
			panic(err)
		}
	}

	cfg := &warden.ClientConfig{}
	paladin.Get("grpc").UnmarshalTOML(cfg)
	log.Info("client配置cfg:%+v", cfg)
	client := warden.NewClient(cfg)
	ctx := context.Background()
	conn, err := client.Dial(ctx, target)
	defer conn.Close()
	if err != nil {
		fmt.Println("client err", err)
		return
	}
	fmt.Println("client connect server success!")
	serverClient := protoModel.NewSearchServiceClient(conn)
	//发送请求
	resp, err := serverClient.Search(ctx, &protoModel.SearchReq{Query: "i am client"})
	if err == nil {
		fmt.Println("rpc success,response:", resp)
	} else {
		fmt.Println("rpc fail,err", err)
	}
}

//测试新的错误包
func TestNewError(t *testing.T)  {

	if _, err := os.Open("no-exist");err!=nil{
		var pathError *os.PathError
		if errors.As(err,&pathError){
			fmt.Println("Failed at path:",pathError.Path)
		}else{
			fmt.Println(err)
		}
	}

	//New
	fmt.Println("test New")
	err1 := errors.New("err1")
	err2 := errors.New("err2")

	//Is
	fmt.Println("test Is")
	fmt.Println(errors.Is(err1,err2))
	fmt.Println(errors.Is(err1,errors.New("err1")))

	//Unwrap
	fmt.Println("test Unwrap")
	e := errors.New("e")
	//通过一个error嵌套另一个error,主要依赖%w
	e1 := fmt.Errorf("e1: %w", e)
	e2 := fmt.Errorf("e2: %w", e1)
	fmt.Println(e2)
	fmt.Println(errors.Unwrap(e2))
	fmt.Println(e1)
	fmt.Println(errors.Unwrap(e1))
}

//测试错误堆栈信心
func TestErrStack(t *testing.T)  {
	err := foo2()
	fmt.Printf("single： %v\n", err)
	fmt.Printf("detail：%+v\n", err)
}

var myerror = xerrors.New("myerror")
func foo() error  {
	return myerror
}
func foo1() error {
	return xerrors.Errorf("foo1 : %w",foo())
}
func foo2() error {
	return xerrors.Errorf("foo2 : %w",foo1())
}
