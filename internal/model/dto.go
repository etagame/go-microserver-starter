/*
@Desc : 2019/6/24 16:27 
@Version : 1.0.0
@Time : 2019/6/24 16:27 
@Author : hammercui
@File : dto
@Company: Sdbean
*/
package model

// ArithmeticRequest define request struct
type ArithmeticRequest struct {
	RequestType string `json:"request_type"`
	A           int    `json:"a"`
	B           int    `json:"b"`
}

// ArithmeticResponse define response struct
type ArithmeticResponse struct {
	Result int   `json:"result"`
	Error  error `json:"error"`
}

type Kratos struct {
	Hello string
}