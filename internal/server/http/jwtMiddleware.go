/*
@Desc : jwt中间件
@Version : 1.0.0
@Time : 2019/6/29 14:54 
@Author : hammercui
@File : jwtMiddleware
@Company: Sdbean
*/
package http

import (
	bm "github.com/bilibili/kratos/pkg/net/http/blademaster"
)

func JwtMidware(c *bm.Context) {
		//fmt.Println("jwt middleware before")
		c.Next()
		//fmt.Println("jwt middleware after")
	}
