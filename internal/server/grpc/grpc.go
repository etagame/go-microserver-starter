/*
@Desc : grpc实例
@Version : 1.0.0
@Time : 2019/7/5 10:50 
@Author : hammercui
@File : grpc
@Company: Sdbean
*/
package grpc

import (
	"github.com/bilibili/kratos/pkg/conf/paladin"
	"github.com/bilibili/kratos/pkg/log"
	"github.com/bilibili/kratos/pkg/net/rpc/warden"
	"go-kit-starter/api"
	"go-kit-starter/internal/service"
)

var WardenConfig struct{
	Server *warden.ServerConfig
}


//return instance
func New(svc *service.Service) * warden.Server{
	if err := paladin.Get("grpc.toml").UnmarshalTOML(&WardenConfig); err != nil {
		if err != paladin.ErrNotExist {
			panic(err)
		}
	}
	log.Info("load grpc config success! %+v",WardenConfig.Server)

	ws := warden.NewServer(WardenConfig.Server)
	ws.Use(customInterceptor())
	// 注意替换这里：
	// RegisterSearchServiceServer方法是在"api"目录下代码生成的
	// 对应proto文件内自定义的service名字，请使用正确方法名替换
	protoModel.RegisterSearchServiceServer(ws.Server(), svc)
	ws, err := ws.Start()
	if err != nil {
		panic(err)
	}
	log.Info("gRPC Start Success!")
	return ws
}

