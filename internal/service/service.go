/*
@Desc : 提供基础service
@Version : 1.0.0
@Time : 2019/6/28 13:47 
@Author : hammercui
@File : service
@Company: Sdbean
*/
package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"go-kit-starter/api"

	"go-kit-starter/internal/dao"
	"github.com/bilibili/kratos/pkg/conf/paladin"
)

// Service service.
type Service struct {
	ac  *paladin.TOML //自定义配置键值对
	dao *dao.Dao
}

// New new a service and return.
func New() (s *Service) {
	var ac = new(paladin.TOML)
	if err := paladin.Watch("application.toml", ac); err != nil {
		panic(err)
	}
	s = &Service{
		ac:  ac,
		dao: dao.New(),
	}
	return s
}

// Ping ping the resource.
func (s *Service) Ping(ctx context.Context) (err error) {
	return s.dao.Ping(ctx)
}

// Close close the resource.
func (s *Service) Close() {
	s.dao.Close()
}

// say hello
func (s *Service)SayHelloURL(ctx context.Context, req *protoModel.HelloReq) (resp *protoModel.HelloResp, err error){
	resp = &protoModel.HelloResp{Content:"test"}
	return
}

func (s *Service)SayHello(ctx context.Context,req *protoModel.HelloReq) (*empty.Empty, error){
	return &empty.Empty{},nil
}

func (s *Service)Search(ctx context.Context, req *protoModel.SearchReq) (resp *protoModel.SearchResp,err error){
	resp = &protoModel.SearchResp{Query:"i am server"}
	return
}